import time
from urllib.parse import urlparse
from logging import getLogger

import tqdm
import scrapy
from scrapy.spiders import Spider,CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.http import Request
from scrapy.selector import Selector
from scrapy.shell import inspect_response

from FunpySpiderSearch.sites.reg007.reg007_item import Reg007Item


class Reg007Spider(Spider):
    """Reg007 爬虫逻辑"""
    name = 'reg007'
    allowed_domains = ['reg007.com']
    custom_settings = {
        # "HTTPERROR_ALLOWED_CODES": [302],
        "LOG_LEVEL": "INFO",
        "DOWNLOADER_MIDDLEWARES": {
            'FunpySpiderSearch.middlewares.Reg007SeleniumMiddleware': 543,
        },
    }
    
    def start_requests(self):
        '''
        采集URL生成
        '''
        with open('./data/广发银行商旅白金信用卡营销-手机号.txt','r') as f:
            telephones = [i.strip() for i in f.readlines()]
            kws = telephones

        with open('./data/crawled1.txt','r',encoding='utf-8') as crawled_file:
            crawled_tels = [tel.strip() for tel in crawled_file.readlines()]
            print(crawled_tels)

        with open('./data/crawled1.txt','a+',encoding='utf-8') as fp:
            count = 0
            for kw in tqdm.tqdm(kws):
                if count%30==0 and count!=0:
                    self.logger.info('sleep 600 seconds')
                    # time.sleep(600)
                if kw not in crawled_tels:
                    print(kw)
                    count +=1
                    fp.write(kw+'\n')
                    yield Request("https://www.reg007.com/search?q=%s"%kw, errback=self.error_back)

    def parse(self, response):
        '''页面解析规则
            HTTP status在200-300之间，会调用callback'''
        results = Selector(response).xpath('//ul/li[@id]')
        # inspect_response(response, self)
        print('-------------',len(results),'-------------')
        for i, result in enumerate(results):
            item = Reg007Item()
            item['url'] = response.request.url
            item['name'] = result.xpath('.//h4/a/text()').extract_first()
            item['cat'] = result.xpath('.//h4/small/a/text()').extract_first()
            item['desc'] = result.xpath('.//p/text()').extract_first()
            print(item)
            yield item

    def error_back(self, failure):
        """HTTP status为非200-300之间，会调用errbackk"""
        from scrapy.spidermiddlewares.httperror import HttpError
        from twisted.internet.error import DNSLookupError
        from twisted.internet.error import TimeoutError, TCPTimedOutError
        # log all failures
        self.logger.error(repr(failure))
        with open('err.txt','w+',encoding='utf-8') as fp:
            print('err_back')
            fp.write(failure.request.url+'\n')
        # in case you want to do something special for some errors,
        # you may need the failure's type:
        if failure.check(HttpError):
            # these exceptions come from HttpError spider middleware
            # you can get the non-200 failure
            response = failure.value.response
            self.logger.error('HttpError on %s', response.url)

        elif failure.check(DNSLookupError):
            # this is the original request
            request = failure.request
            self.logger.error('DNSLookupError on %s', request.url)

        elif failure.check(TimeoutError, TCPTimedOutError):
            request = failure.request
            self.logger.error('TimeoutError on %s', request.url)
