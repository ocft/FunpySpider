import time
from urllib.parse import urlparse

import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.http import Request
from scrapy.selector import Selector

from FunpySpiderSearch.sites.baidu.baidu_item import BaiduItem


class BaiduSpider(CrawlSpider):
    """百度爬虫逻辑"""
    name = 'baidu'
    allowed_domains = ['baidu.com']
    # 覆盖settings中的配置
    custom_settings = {
        "COOKIES_ENABLED" : False,
        "DOWNLOAD_DELAY": 1.5,
        "DOWNLOADER_MIDDLEWARES": {
            'scrapy.downloadermiddlewares.retry.RetryMiddleware': 90,
            'FunpySpiderSearch.middlewares.FunpyspidersearchDownloaderMiddleware': 543,
        }
    }
    # 链接提取规则
    rules = (
        Rule(LinkExtractor(allow=(r'&pn=[1-3]0'), restrict_xpaths='//a[@class="n"][contains(., ">")]'),callback='parse_item', follow=True),
    )

    # def __init__(self, keyword=None,  *args, **kwargs):
    #     """可以通过命令行-a传参实现初始化操作
    #         scrapy crawl baidu -a keyword=我是关键词"""
    #     pass

    def start_requests(self):
        '''
        This method will use searchwords to init start urls
        '''
        kws = ['马云']
        for i, kw in enumerate(kws):
            yield Request("http://www.baidu.com/s?wd=%s"%kw)

    def parse_start_url(self, response):
        """ 如果起始的url解析方式有所不同，
            可以重写本方法来解析第一个url返回的Response
        """
        pass

    def parse_item(self, response):
        '''页面数据解析规则'''
        results = Selector(response).xpath('//div[contains(@class, "c-container") and not(contains(@class, "result-op"))]')
        kw = Selector(response).xpath('//input[@id = "kw"]/@value').extract()[0]
        current_page = Selector(response).xpath('string(//div[@id="page"]//strong//span[@class="pc"])').extract()[0]
        for i, result in enumerate(results):
            item = BaiduItem()
            item['domain'] = self.allowed_domains[0]
            item['kw'] = kw
            item['title'] = result.xpath('string(.//h3//a)').extract()[0]
            item['url'] = result.xpath('.//h3/a/@href').extract()[0]
            item['desc'] = result.xpath('string(.//div[@class="c-abstract"])').extract()[0]
            urlstr = 'http://' + result.xpath('string(.//div[@class="f13"]//span)').extract()[0]
            parsed_uri = urlparse(urlstr)
            item['sourceurl'] = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)

            # 根据active的a标签的值来排名
            if current_page:
                page = current_page if int(current_page) >= 10 else ('0%s' % (current_page,))
            else:
                page = '01'
            rank = str(i) if i >= 10 else ('0%d' %(i,))
            item['rank'] = page + rank
            item['content'] = ''
            yield item
