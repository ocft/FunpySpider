from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from datetime import datetime
from scrapy_selenium import SeleniumRequest

from FunpySpiderSearch.sites.lagou.lagou_Item import LagouJobItem, LagouJobItemLoader
from FunpySpiderSearch.utils.common import get_md5


class LagouJobspider(CrawlSpider):
    name = 'lagou'
    allowed_domains = ['www.lagou.com']
    start_urls = ['https://www.lagou.com/']
    agent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/" \
            "537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36"
    custom_settings = {
        "COOKIES_ENABLED": True,
        "DOWNLOAD_DELAY": 3,
        'DEFAULT_REQUEST_HEADERS': {
            'Accept': 'application/json, text/javascript, */*; q=0.01',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'zh-CN,zh;q=0.8',
            'Connection': 'keep-alive',
            'Cookie': 'JSESSIONID=ABAAAECABAIAAFEC4291ADA1E6FB5298D28D9D9D897C6A9; user_trace_token=20200309135502-5b63e3d4-9ae7-4aa2-97a1-41f4c30250d4; sajssdk_2015_cross_new_user=1; sensorsdata2015session=%7B%7D; sensorsdata2015jssdkcross=%7B%22distinct_id%22%3A%22170bddc6dea53-0d4eaad66b864d-4313f6a-1327104-170bddc6deb645%22%2C%22%24device_id%22%3A%22170bddc6dea53-0d4eaad66b864d-4313f6a-1327104-170bddc6deb645%22%2C%22props%22%3A%7B%22%24latest_traffic_source_type%22%3A%22%E8%87%AA%E7%84%B6%E6%90%9C%E7%B4%A2%E6%B5%81%E9%87%8F%22%2C%22%24latest_referrer%22%3A%22https%3A%2F%2Fwww.google.com%2F%22%2C%22%24latest_referrer_host%22%3A%22www.google.com%22%2C%22%24latest_search_keyword%22%3A%22%E6%9C%AA%E5%8F%96%E5%88%B0%E5%80%BC%22%2C%22%24os%22%3A%22Windows%22%2C%22%24browser%22%3A%22Chrome%22%2C%22%24browser_version%22%3A%2280.0.3987.132%22%7D%7D; WEBTJ-ID=20200309143309-170bdff4c21567-0251283c60b5aa-4313f6a-1327104-170bdff4c2487; PRE_UTM=; PRE_LAND=https%3A%2F%2Fwww.lagou.com%2F; LGUID=20200309143309-f8b69b6e-b58e-4776-8c47-fb69356a5847; Hm_lvt_4233e74dff0ae5bd0a3d81c6ccf756e6=1583735591; _ga=GA1.2.1247804350.1583735591; _gat=1; _gid=GA1.2.827230188.1583735591; LGSID=20200309143310-53a9d1f5-349f-42ec-8b78-6e164a43bc21; PRE_HOST=www.google.com; PRE_SITE=https%3A%2F%2Fwww.google.com%2F; gate_login_token=f37c29710e7a7fd234083e5a01877be992aea59f36843c82; LG_LOGIN_USER_ID=f9e1470587fe005f03e38d1fb1ca9c30df3ab71a1b7564cd; LG_HAS_LOGIN=1; _putrc=EDA6CE1B5C6CA93F; login=true; unick=491315091%40qq.co; showExpriedIndex=1; showExpriedCompanyHome=1; showExpriedMyPublish=1; hasDeliver=0; index_location_city=%E4%B8%8A%E6%B5%B7; privacyPolicyPopup=false; X_HTTP_TOKEN=b1e40a50350734af0165373851822ce8c47ddcbd6a; LGRID=20200309143331-75305711-7a0b-41c2-8d97-8c2bf53eff38; Hm_lpvt_4233e74dff0ae5bd0a3d81c6ccf756e6=1583735613',
            'Host': 'www.lagou.com',
            'Origin': 'https://www.lagou.com',
            'Referer': 'https://www.lagou.com/',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                          '(KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',
        }
    }

    rules = (
        Rule(LinkExtractor(allow=r'jobs/\d+.html'), callback='parse_content', follow=True),
    )

    @staticmethod
    def parse_content(response):
        item_loader = LagouJobItemLoader(item=LagouJobItem(), response=response)
        item_loader.add_css("title", ".job-name::attr(title)")
        item_loader.add_value("url", response.url)
        item_loader.add_value("url_object_id", get_md5(response.url))
        item_loader.add_css("salary_min", ".job_request .salary::text")
        item_loader.add_xpath("job_city", "//*[@class='job_request']/p/span[2]/text()")
        item_loader.add_xpath("work_years_min", "//*[@class='job_request']/p/span[3]/text()")
        item_loader.add_xpath("degree_need", "//*[@class='job_request']/p/span[4]/text()")
        item_loader.add_xpath("job_type", "//*[@class='job_request']/p/span[5]/text()")
        item_loader.add_css("tags", '.position-label li::text')
        item_loader.add_css("publish_time", ".publish_time::text")
        item_loader.add_css("job_advantage", ".job-advantage p::text")
        item_loader.add_css("job_desc", ".job_bt div")
        item_loader.add_css("job_addr", ".work_addr")
        item_loader.add_css("company_name", "#job_company dt a img::attr(alt)")
        item_loader.add_css("company_url", "#job_company dt a::attr(href)")
        item_loader.add_value("crawl_time", datetime.now())

        job_item = item_loader.load_item()

        return job_item
