# -*- coding: utf-8 -*-

# Scrapy settings for FunpySpiderSearch project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://doc.scrapy.org/en/latest/topics/settings.html
#     https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://doc.scrapy.org/en/latest/topics/spider-middleware.html
from config import MYSQL_USER, MYSQL_PASSWORD, MYSQL_DBNAME

BOT_NAME = 'FunpySpiderSearch'

SPIDER_MODULES = ['FunpySpiderSearch.spiders']
NEWSPIDER_MODULE = 'FunpySpiderSearch.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
# USER_AGENT = 'FunpySpiderSearch (+http://www.yourdomain.com)'

# Obey robots.txt rules
ROBOTSTXT_OBEY = False

# Configure maximum concurrent requests performed by Scrapy (default: 16)
# CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See https://doc.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
# DOWNLOAD_DELAY = 3
# The download delay setting will honor only one of:
# CONCURRENT_REQUESTS_PER_DOMAIN = 16
# CONCURRENT_REQUESTS_PER_IP = 16

# Disable cookies (enabled by default)
COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
# TELNETCONSOLE_ENABLED = False

# Override the default request headers:
# DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
# }

# Enable or disable spider middlewares
# See https://doc.scrapy.org/en/latest/topics/spider-middleware.html
# SPIDER_MIDDLEWARES = {
#     "scrapy_deltafetch.DeltaFetch": 100,
#     'FunpySpiderSearch.middlewares.FunpyspidersearchSpiderMiddleware': 543,
# }
# DELTAFETCH_ENABLED = True
# Enable or disable downloader middlewares
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
# Retry many times since proxies often fail
# 下载中间件是处于引擎(crawler.engine)和下载器(crawler.engine.download())之间的一层组件，
# 可以用来修改Request和Response。
RETRY_TIMES = 3
# Retry on most error codes since proxies fail for different reasons
RETRY_HTTP_CODES = [500, 503, 504, 400, 403, 404, 408]
CRAWLERA_ENABLED = True
CRAWLERA_USER = '注册/购买的UserKey'
CRAWLERA_PASS = '注册/购买的Password'
DOWNLOADER_MIDDLEWARES = {
    # 'scrapy.downloadermiddlewares.retry.RetryMiddleware': 90,
    # 'scrapy_selenium.SeleniumMiddleware': 800,
    # 'FunpySpiderSearch.middlewares.RandomUserAgentMiddleware': 543,
    # 'FunpySpiderSearch.middlewares.FunpyspidersearchDownloaderMiddleware': 543,
    # 'FunpySpiderSearch.middlewares.SeleniumMiddleware': 543,
    # 'scrapy_crawlera.CrawleraMiddleware': 600
}
  
# Enable or disable extensions
# See https://doc.scrapy.org/en/latest/topics/extensions.html
# EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
# }

# Configure item pipelines
# See https://doc.scrapy.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
    'FunpySpiderSearch.pipelines.FunpyspidersearchPipeline': 300,
    # 'FunpySpiderSearch.pipelines.MysqlTwistedPipeline': 400,
    # 'FunpySpiderSearch.pipelines.ElasticSearchPipeline': 500,
}

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/autothrottle.html
# AUTOTHROTTLE_ENABLED = True
# The initial download delay
# AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
# AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
# AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
# AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
# HTTPCACHE_ENABLED = True
# HTTPCACHE_EXPIRATION_SECS = 0
# HTTPCACHE_DIR = 'httpcache'
# HTTPCACHE_IGNORE_HTTP_CODES = []
# HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'

# 数据库配置信息
MYSQL_HOST = "127.0.0.1"
MYSQL_DBNAME = MYSQL_DBNAME
MYSQL_USER = MYSQL_USER
MYSQL_PASSWORD = MYSQL_PASSWORD

# 数据库时间格式
SQL_DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
SQL_DATE_FORMAT = "%Y-%m-%d"

RANDOM_UA_TYPE = "random"
