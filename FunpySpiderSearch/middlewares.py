# -*- coding: utf-8 -*-

# Define here the models for your spider middleware
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/spider-middleware.html

import random
import time
import json
from logging import getLogger

from fake_useragent import UserAgent
from scrapy import signals
from scrapy.core.downloader.handlers.http11 import TunnelError
from scrapy.http import HtmlResponse
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from twisted.internet import defer
from twisted.internet.error import (ConnectError, ConnectionDone,
                                    ConnectionLost, ConnectionRefusedError,
                                    DNSLookupError, TCPTimedOutError,
                                    TimeoutError)
from twisted.web.client import ResponseFailed

options = webdriver.ChromeOptions()
options.add_argument("start-maximized")
options.add_experimental_option("excludeSwitches", ["enable-automation"])
options.add_experimental_option('useAutomationExtension', False)
options.add_argument("--disable-images")
options.add_argument('lang=zh_CN.UTF-8')
options.add_argument('--ignore-certificate-errors')

# options.setPageLoadStrategy(PageLoadStrategy.NONE); // https://www.skptricks.com/2018/08/timed-out-receiving-message-from-renderer-selenium.html
# options.add_argument("enable-automation"); // https://stackoverflow.com/a/43840128/1689770
# options.add_argument("--headless")
# options.add_argument("--no-sandbox"); //https://stackoverflow.com/a/50725918/1689770
# options.add_argument("--disable-infobars"); //https://stackoverflow.com/a/43840128/1689770
# options.add_argument("--disable-dev-shm-usage"); //https://stackoverflow.com/a/50725918/1689770
# options.add_argument("--disable-browser-side-navigation"); //https://stackoverflow.com/a/49123152/1689770
# options.add_argument("--disable-gpu"); //https://stack


class RandomUserAgentMiddleware(object):
    '''
    随机更换user-agent
    '''

    def __init__(self, crawler):
        super(RandomUserAgentMiddleware, self).__init__()
        self.ua = UserAgent()
        # 可读取在settings文件中的配置，来决定开源库ua执行的方法，默认是random，也可是ie、Firefox等等
        self.ua_type = crawler.settings.get("RANDOM_UA_TYPE", "desktop")

    def process_request(self, request, spider):
        def get_ua():
            return getattr(self.ua, self.ua_type)
        request.headers.setdefault('User-Agent', get_ua())
        return None

    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler)


class RandomCookiesMiddleware(object):
    def __init__(self, cookies):
        self.cookies = cookies

    @classmethod
    def from_crawler(cls, crawler):
        # 从设置文件中获取cookies列表
        # 也可以从数据库中取
        cookies = crawler.settings['COOKIES']
        return cls(cookies)

    def process_request(self, request, spider):
        # 随机获取一个cookies
        cookie = random.choice(self.cookies)
        if cookie:
            request.cookies = cookie


class SeleniumMiddleware(object):
    def __init__(self):
        self.logger = getLogger(__name__)
        self.browser = webdriver.Chrome()
        self._set_option()
        
        # self.wait = WebDriverWait(self.browser, self.timeout)

    def __del__(self):
        self.browser.close()

    def _login(self):
        pass

    def _pre_actions(self):
        pass

    def _set_option(self):
        """浏览器初始化设置"""
        self.browser.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {
            "source": """
        Object.defineProperty(navigator, 'webdriver', {
            get: () => undefined
        })
        """
        })
        self.browser.execute_cdp_cmd("Network.enable", {})
        self.browser.execute_cdp_cmd("Network.setExtraHTTPHeaders", {
            "headers": {"User-Agent": "browser1"}})

    @classmethod
    def from_crawler(cls, crawler):
        return cls(timeout=crawler.settings.get('SELENIUM_TIMEOUT', 60))


class Reg007SeleniumMiddleware(SeleniumMiddleware):
    def __init__(self, timeout=60):
        super().__init__()
        self.timeout = timeout
        self._login()
        self.browser.set_page_load_timeout(self.timeout)
        self.browser.set_script_timeout(self.timeout)

    def _login(self):
        '''预登陆'''
        self.browser.get("https://www.reg007.com/account/signin")
        # self.browser.implicitly_wait(1.5)
        time.sleep(2)
        input('请预登录, 登录后点击回车继续运行')
        
    def _pre_actions(self):
        """预执行操作"""
        WebDriverWait(self.browser, 70, 2).until(EC.visibility_of_element_located((By.ID,'li_submit_site')))
    
    def _captcha(self, keyword):
        if keyword in self.browser.page_source:
            self.logger.info('跳转到打码页面！')
            input("发现了验证码，请输入验证码后，按回车键继续运行！")
            self.browser.refresh()
        else:
            pass

    def process_request(self, request, spider):
        """
        用预登陆后的Chrome抓取页面
        """
        self.logger.debug('Chrome is Starting')
        try:
            # options.add_argument('user-agent="%s"' % self.ua.random)
            self.browser.get(request.url)
            # self.browser.implicitly_wait(2)
            time.sleep(2)
            self._captcha('人机识别')
            self._pre_actions()
            # WebDriverWait(self.browser, 80, 2).until(EC.visibility_of_element_located((By.ID,'li_submit_site')))
            return HtmlResponse(url=request.url, body=self.browser.page_source, request=request, encoding='utf-8', status=200)
        except TimeoutException:
            self.logger.error('Middleware TimeoutException')
            return HtmlResponse(url=request.url, body=self.browser.page_source, request=request, encoding='utf-8', status=200)
        except Exception as e:
            self.logger.error('other error')
            self.logger.error(repr(e))
            # 500会进入重试并且被parse忽略
            return HtmlResponse(url=request.url, status=500, request=request)


class ProcessAllExceptionMiddleware(object):
    ALL_EXCEPTIONS = (defer.TimeoutError, TimeoutError, DNSLookupError,
                      ConnectionRefusedError, ConnectionDone, ConnectError,
                      ConnectionLost, TCPTimedOutError, ResponseFailed,
                      IOError, TunnelError)

    def process_response(self, request, response, spider):
        # 捕获状态码为40x/50x的response
        if str(response.status).startswith('4') or str(response.status).startswith('5'):
            # 随意封装，直接返回response，spider代码中根据url==''来处理response
            response = HtmlResponse(url='')
            return response
        # 其他状态码不处理
        return response

    def process_exception(self, request, exception, spider):
        # 捕获几乎所有的异常
        if isinstance(exception, self.ALL_EXCEPTIONS):
            # 在日志中打印异常类型
            print('Got exception: %s' % (exception))
            # 随意封装一个response，返回给spider
            response = HtmlResponse(url='exception')
            return response
        print('not contained exception: %s' % exception)


class FunpyspidersearchSpiderMiddleware(object):
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the spider middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_spider_input(self, response, spider):
        # Called for each response that goes through the spider
        # middleware and into the spider.

        # Should return None or raise an exception.
        return None

    def process_spider_output(self, response, result, spider):
        # Called with the results returned from the Spider, after
        # it has processed the response.

        # Must return an iterable of Request, dict or Item objects.
        for i in result:
            yield i

    def process_spider_exception(self, response, exception, spider):
        # Called when a spider or process_spider_input() method
        # (from other spider middleware) raises an exception.

        # Should return either None or an iterable of Response, dict
        # or Item objects.
        pass

    def process_start_requests(self, start_requests, spider):
        # Called with the start requests of the spider, and works
        # similarly to the process_spider_output() method, except
        # that it doesn’t have a response associated.

        # Must return only requests (not items).
        for r in start_requests:
            yield r

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)


class FunpyspidersearchDownloaderMiddleware(object):
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the downloader middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_request(self, request, spider):
        # Called for each request that goes through the downloader
        # middleware.

        # Must either:
        # - return None: continue processing this request
        # - or return a Response object
        # - or return a Request object
        # - or raise IgnoreRequest: process_exception() methods of
        #   installed downloader middleware will be called
        return None

    def process_response(self, request, response, spider):
        # Called with the response returned from the downloader.

        # Must either;
        # - return a Response object
        # - return a Request object
        # - or raise IgnoreRequest
        return response

    def process_exception(self, request, exception, spider):
        # Called when a download handler or a process_request()
        # (from other downloader middleware) raises an exception.

        # Must either:
        # - return None: continue processing this exception
        # - return a Response object: stops process_exception() chain
        # - return a Request object: stops process_exception() chain
        pass

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)
