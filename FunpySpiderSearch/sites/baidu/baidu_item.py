import re

import scrapy
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, MapCompose, Join


class BaiduItemLoader(ItemLoader):
    default_output_processor = TakeFirst()


class BaiduItem(scrapy.Item):

    domain = scrapy.Field() # baidu.com / google.com
    kw = scrapy.Field() # 关键词
    title = scrapy.Field() # 标题
    url = scrapy.Field() # 链接
    desc = scrapy.Field() # 简介
    sourceurl = scrapy.Field() # 文章来源
    rank = scrapy.Field() # 排名
    content = scrapy.Field() # 正文内容