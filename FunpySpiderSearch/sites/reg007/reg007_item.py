import re

import scrapy
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, MapCompose, Join


class Reg007ItemLoader(ItemLoader):
    default_output_processor = TakeFirst()


class Reg007Item(scrapy.Item):
    url = scrapy.Field()
    name = scrapy.Field()
    cat = scrapy.Field()
    desc = scrapy.Field()